import time
import datetime
import os

import ROOT
import numpy as np
import math as math
import linecache
import sys
#from sys import exit
import glob

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()...
def sqrt(x):
  return np.sqrt(x)





# From 
# https://www.reddit.com/r/Watches/comments/64lw33/tool_i_couldnt_find_a_free_watch_trackerlogger_so/
# https://codedump.io/share/RzKxNvJjFNEd/1/watchloggerpy
# Modified and torn up.


### Print current collection and associated log prefixes
WatchLogs = ['skx', 'vostokk', 'vostokb', 'raketa', 'casio']
WatchNames = ['Seiko SKX007', 'Vostok Amphibia, black', 'Vostok Amphibia, blue', 'Raketa Big Zero', 'Casio A158W']

# All this here to indent the print lines so they line up...
lenWatchNames = [len(i) for i in WatchNames]
maxlenWatchNames = max(lenWatchNames)
spaces = [maxlenWatchNames-i for i in lenWatchNames]
indent = [''.ljust(i) for i in spaces]


fileext = 'log'
filedir = 'Logs'

### While loop to ask to log another time/watch:
# From https://stackoverflow.com/questions/14907067/how-do-i-restart-a-program-based-on-user-input
while True: 

  print ''
  for i,n,l in zip(indent,WatchNames, WatchLogs):
    print i, n, '=', '\"'+l+'\"'
  print ''


  ### Print existing logs
  files = filter(os.path.isfile, glob.glob(filedir+'/*.'+fileext))
  files.sort(key=lambda x: os.path.getmtime(x))
  print ' Currently tracked (oldest to newest):'
  print ' ', [ff.replace(filedir+'/','') for ff in [f.replace('.'+fileext,'') for f in files]]
  print '' 


  ###  Log single watch
  WatchLogs_this = raw_input('log = ')
  #WatchNames_this = WatchNames[WatchLogs.index(WatchLogs_this)]
  # String numbers from WatchLogs_this so skx, skx2, skx3, etc. all pull "Seiko SKX007"
  # From https://stackoverflow.com/questions/12851791/removing-numbers-from-string
  s = WatchLogs_this
  s = ''.join([i for i in s if not i.isdigit()])
  WatchNames_this = WatchNames[WatchLogs.index(s)]

  log = filedir+'/'+WatchLogs_this+'.'+fileext

  hhmmss = raw_input('hh:mm:ss (24 hr): ')
  [hour, minute, second] = [int(x) for x in hhmmss.split(':')]
  date = datetime.date.today()
  date = datetime.datetime(date.year,date.month,date.day,hour,minute,second)

  RealDate = str(date.year)+str(date.month).zfill(2)+str(date.day).zfill(2)
  RealTime = hhmmss
  RealTimeUTC = time.time()
  WatchTimeUTC = time.mktime(date.timetuple())
  DiffTimeUTC = round(WatchTimeUTC - RealTimeUTC,1)


  if not os.path.exists(log):
    f = open(log,'w')
    f.write('Date \t\tTime \t\tWatch (UTC) \tReal (UTC) \tDifference (s)')
    f.close()

  with open(log,'a') as f:
    f.write('\n'+RealDate +'\t'+ str(RealTime) +'\t'+ str(WatchTimeUTC) +'\t'+ str(RealTimeUTC) +'\t'+str(DiffTimeUTC) )

  print ''
  with open(log,'r') as f:
    for line in f:
      print line
  print ''


  while True:
    answer = raw_input('Run again? (y/n): ')
    if answer in ('y', 'n'):
      break
    print 'Invalid input.'
  if answer == 'y':
    continue
  else:
    break

