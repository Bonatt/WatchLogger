### Logger and plotter for determing watch or clock accuracy

This project was created out of "necessity". I "needed" to determine the timekeeping rates of my watches. 
There are many ways to do this. Without the access to a [timegrapher](http://www.wristtimes.com/blog-1/2014/6/24/how-to-use-a-timegrapher) one must rely instead on any of the less precise methods:
1. Manually log the time (via memory, pen, vi, etc.)
2. Use of some application that photographs the watchface.
3. Use of some application that logs user input.

These all involve comparing the time of the watch to a known, accurate time source (and then not adjusting the time again). 
Comparing the deviation from that source over some time period yields the rate of the watch.

I originally employed method 1 because it was easy. I set the time correctly (or as close as possible),
and after a minimum of one day I could see how fast or slow my watch was. 
This rate is traditionally displayed as the difference from the actual time, i.e. +/- some seconds per day.

The problem, however, is that one must wait a relatively long time to determine the rate, and it never seems to be all that accurate. 
The use of more precise trackers, such as the one I've created here, allows one to determine the rate after mere hours instead of days. 
It's just a simple log, plot, and fit program. It compares the manually entered time to the computers UTC. 


### Use

Let's say I wanted to determine the rate of my Seiko SKX007K. It's magnetized and I've dropped it a few times so 
it's running pretty fast. But how fast? 
If I wanted to add a new datum point to skx6, I'd run logger.py, enter skx6 for the log (most-recent log file for my SKX), 
and enter the time displayed by my SKX (I usually pre-typed the time for precision): 
```
>>> python logger.py

           Seiko SKX007 = "skx"
 Vostok Amphibia, black = "vostokk"
  Vostok Amphibia, blue = "vostokb"
        Raketa Big Zero = "raketa"
            Casio A158W = "casio"

 Currently tracked (oldest to newest):
  ['casio', 'raketa', 'raketa2', 'skx', 'skx2', 'skx3', 'skx4', 'skx5', 'skx6', 'vostokk', 'vostokk2', 'vostokk3']

log = skx6
hh:mm:ss (24 hr): 13:00:00
Date            Time            Watch (UTC)     Real (UTC)      Difference (s)
20170605        13:40:00        1496684400.0    1496684413.87   -13.9
20170605        13:40:15        1496684415.0    1496684429.08   -14.1
20170605        13:40:40        1496684440.0    1496684453.78   -13.8
...

Run again? (y/n): n
```


Now we can determine and visualize the rate. 
I originally created plotterMULTI.py to plot multiple logs but that eventually broke so now I only use plotterONE.py:
```
>>> python plotterONE.py
 
          Seiko SKX007 = "skx"
 Vostok Amphibia, black = "vostokk"
  Vostok Amphibia, blue = "vostokb"
        Raketa Big Zero = "raketa"
            Casio A158W = "casio"

 Currently tracked (oldest to newest):
  ['casio', 'raketa', 'raketa2', 'skx', 'skx2', 'skx3', 'skx4', 'skx5', 'vostokk', 'vostokk2', 'vostokk3', 'skx6']

plot = skx6

 FCN=809.611 FROM MIGRAD    STATUS=CONVERGED      34 CALLS          35 TOTAL
                     EDM=3.9916e-20    STRATEGY= 1      ERROR MATRIX ACCURATE
  EXT PARAMETER                                   STEP         FIRST
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE
   1  p0          -1.22866e+01   1.62566e-01   2.00658e-03  -1.69971e-09
   2  p1           6.56632e-04   3.10450e-06   3.14673e-07   4.19573e-06

Info in <TCanvas::Print>: png file Plots/skx6.png has been created
```

![SKX6](Plots/skx6.png)

The default fitting function is _y = a + bx_, where _y_ is accuracy and _x_ is time. Variable _b_ is the parameter of interest: seconds per day. 
Variable _a_ is just an offset because all my watches are [nonhacking](https://en.wikipedia.org/wiki/Hack_watch). 
So my SKX is (was) almost a minute fast a day. 
Notice the large Chi2. This isn't all that great of a fit but I promise most are better. Here some more examples:
![Casio](Plots/casio.png)
![Raketa](Plots/raketa.png)

For reference, [COSC](https://en.wikipedia.org/wiki/COSC) specification for a mechanical movement is -4/+6 s/d, and Rolex's "Superlative Chronometer" specification is +/- 2 s/d. For a 30 year old watch from the USSR, my Raketa is doing pretty well!
