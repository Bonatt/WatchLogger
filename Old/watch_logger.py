import time
import datetime
import os

import ROOT
import numpy as np
import math as math
import linecache
import sys


# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()...
def sqrt(x):
  return np.sqrt(x)

#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(1);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();














# From 
# https://www.reddit.com/r/Watches/comments/64lw33/tool_i_couldnt_find_a_free_watch_trackerlogger_so/
# https://codedump.io/share/RzKxNvJjFNEd/1/watchloggerpy
# Modified and torn up.
# I divided print statements by 60 to correctly give seconds. And commentede


watches_log = ['skx', 'vostok_k', 'vostok_b', 'raketa', 'casio']
watches_name = ['Seiko SKX007', 'Vostok Amphibia, black', 'Vostok Amphibia, blue', 'Raketa Big Zero', 'Casio A158W']

print ''
print ' Watch name = log suffix'
for n,l in zip(watches_name, watches_log):
  print ' ', n, '=', '\"'+l+'\"'
print ''


watches_log_this = raw_input('Enter watch suffix to log: ')
watches_name_this = watches_name[watches_log.index(watches_log_this)]

logn = 'time_log_'
log = logn+str(watches_log_this)+'.txt'
#touch log

'''
if not os.path.exists(log):
    f = open(log,'w')
    #f.close()
    #f = open(log,'a')
    #t = time.time()
    #f.write(str(t)+' '+str(t))
    f.write('Date \t\tTime \t\tRealUTC \tWatchUTC \tDifferenceUTC \tRateUTC')
    f.close()
'''
'''
with open(log,'r') as fh:
    for line in fh:
        pass
    last = line
#sys_time,last_watch_time = map(float,last.split())
'''

hhmmss = raw_input('Enter 24 hr watch time (hh:mm:ss): ')
[hour, minute, second] = [int(x) for x in hhmmss.split(':')]
date = datetime.date.today()
date = datetime.datetime(date.year,date.month,date.day,hour,minute,second)


current_date = str(date.year)+str(date.month).zfill(2)+str(date.day).zfill(2)
current_time = hhmmss
current_time_UTC = time.time()

watch_time_UTC = time.mktime(date.timetuple())

diff_time_UTC = round(watch_time_UTC - current_time_UTC,1)

'''
print ' '+watches_name_this+':'
print (' {0} seconds fast from UTC on {1}').format(round(watch_time-current_time,date))
print (' {0} seconds diff from last measure on {1}').format(round(last_watch_time+(current_time-sys_time)-watch_time,1))
watch_time_diff = last_watch_time+(current_time-sys_time)-watch_time
#percentage =watch_time_diff * 1/((current_time-sys_time)/(24*60*60))
#print ('your watch is expected to deviated {0} seconds/24hours').format(percentage)
'''


if not os.path.exists(log):
  f = open(log,'w')
  f.write('Date \t\tTime \t\tWatch (UTC) \tReal (UTC) \tDifference (s) \tRate \tRateUnc (s/d) ')
  f.write('\n'+current_date +'\t'+ str(current_time) +'\t'+ str(current_time_UTC) +'\t'+ str(current_time_UTC) +'\t'+str(0.0)+'\t\t'+str(0.0)+'\t'+str(0.0))
  f.close()

with open(log,'r') as fh:
    for line in fh:
        pass
    previous = line
#sys_time,previous_watch_time = map(str,previous.split())
d,t, previous_watch_time_UTC, previous_current_time_UTC, previous_diff_time_UTC,r,runc = map(str,previous.split())

previous_current_time_UTC = float(previous_current_time_UTC)
previous_watch_time_UTC = float(previous_watch_time_UTC)
previous_diff_time_UTC = float(previous_diff_time_UTC)

#rate_time_UTC = float(previous_watch_time_UTC)+(current_time_UTC-float(sys_time_UTC))-watch_time_UTC
#rate_time_UTC = previous_watch_time_UTC + (current_time_UTC-sys_time_UTC) - watch_time_UTC
#rate_time_UTC = diff_time_UTC/(current_time_UTC-previous_current_time_UTC)

#rate_time_UTC = (diff_time_UTC-previous_diff_time_UTC)/(current_time_UTC-previous_current_time_UTC)
#rate_time_UTC = int(round((watch_time_UTC-previous_watch_time_UTC)/(current_time_UTC-previous_current_time_UTC),4))
#rate_time_UTCUnc = rate_time_UTC*np.sqrt( (0.1/1.0)**2 + (0.1/(current_time_UTC-previous_current_time_UTC))**2)
### Round to int because of 0.1 second unc on first watch measurement, then on second. Really, just accurate to 1 second here.

#rate_time_UTC = (watch_time_UTC-previous_watch_time_UTC)/((current_time_UTC-previous_current_time_UTC)/86400.)
#rate_time_UTC = (diff_time_UTC-previous_diff_time_UTC)/((current_time_UTC-previous_current_time_UTC)/86400.)

#rate_time_UTC = diff_time_UTC/((current_time_UTC-previous_current_time_UTC)/86400.)
rate_time_UTC = (diff_time_UTC-previous_diff_time_UTC)/((current_time_UTC-previous_current_time_UTC)/86400.)
#rate_time_UTCUnc = rate_time_UTC*np.sqrt(((current_time_UTC-previous_current_time_UTC)/86400.))/((current_time_UTC-previous_current_time_UTC)/86400.)
rate_time_UTCUnc = np.sqrt((0.5/1.0)**2 + (np.sqrt(((current_time_UTC-previous_current_time_UTC)/86400.))/((current_time_UTC-previous_current_time_UTC)/86400.))**2 )



with open(log,'a') as f:
  f.write('\n'+current_date +'\t'+ str(current_time) +'\t'+ str(watch_time_UTC) +'\t'+ str(current_time_UTC) +'\t'+str(diff_time_UTC) +'\t\t'+str(round(rate_time_UTC,1))+'\t'+str(abs(round(rate_time_UTCUnc,1))))


print ''
#print ' '+watches_name_this+':'
with open(log,'r') as f:
  for line in f:
    print line
print ''














#Date, RealUTC, WatchUTC, DiffUTC  = np.genfromtxt(log, skip_header=1, usecols=(0,2,3,4), unpack=True)
#Time = np.genfromtxt(log, skip_header=1, usecols=1, dtype=str)
RealUTC, Diff_s, Rate_spd, RateUnc_spd = np.genfromtxt(log, skip_header=3, usecols=(3,4,5,6), unpack=True)

# Shift UTC value to make first index zero. Error is zero.
RealUTC = [(i-RealUTC[0])/86400. for i in RealUTC]
RealUTCUnc = np.zeros(len(RealUTC))

# Error is half second?
Diff_sUnc = np.empty(len(Diff_s))
Diff_sUnc.fill(0.5)



### Make a plot. Later one is "better" (simpler).
'''
xRange1 = RealUTC[0]-(RealUTC[-1]-RealUTC[0])*0.05
xRange2 = RealUTC[-1]+(RealUTC[-1]-RealUTC[0])*0.05
yRange1 = (min(Rate_spd)-max(RateUnc_spd))*1.1
yRange2 = (max(Rate_spd)+max(RateUnc_spd))*1.1

c = ROOT.TCanvas('c', 'c', 1366, 720)
pad = ROOT.TPad('pad', '', 0,0,1,1)
pad.SetGrid()
pad.Draw()
pad.cd()

gRate = ROOT.TGraphErrors(len(RealUTC), np.array(RealUTC), np.array(Rate_spd), np.array(RealUTCUnc), np.array(RateUnc_spd))
gRate.SetTitle(';Elapsed time (d);Relative rate (s/d)')
gRate.GetXaxis().SetLimits(xRange1, xRange2)
gRate.GetYaxis().SetRangeUser(yRange1, yRange2)
gRate.SetLineColor(ROOT.kViolet)
gRate.SetLineWidth(2)
gRate.Draw()

gDiff = ROOT.TGraph(len(RealUTC), np.array(RealUTC), np.array(Diff_s))
#gDiff.SetLineStyle(ROOT.kDashed)
gDiff.SetLineColor(ROOT.kCyan)
gDiff.SetLineWidth(2)
gDiff.Draw('same')

#c.cd()
#overlay = ROOT.TPad('overlay', '', 0,0,1,1)
#overlay.SetFillStyle(0)
#overlay.SetFillColor(0)
#overlay.SetFrameFillStyle(0)
#overlay.Draw()
#overlay.cd()

xmin = pad.GetUxmin()
ymin = -120
xmax = pad.GetUxmax()
ymax = 120

#hframe = overlay.DrawFrame(xmin,ymin,xmax,ymax)
#hframe.GetXaxis().SetLabelOffset(99)
#hframe.GetYaxis().SetLabelOffset(99)
#gDiff.Draw()

#axis3 = ROOT.TGaxis(xRange2, yRange1, xRange2, yRange2, -30, 30, 510,"L+")
axis3 = ROOT.TGaxis(xmax, ymin, xmax, ymax, ymin,ymax, 510,"L+")
axis3.SetTitle('Accuracy (s)')
axis3.SetTitleFont(132)
axis3.SetTitleSize(0.04)
axis3.SetTitleOffset(0.85)
axis3.SetLabelFont(132)
axis3.SetLabelSize(0.015)#25)
axis3.SetLineColor(ROOT.kGray+2)
axis3.SetLabelColor(ROOT.kGray+2)
axis3.SetTitleColor(ROOT.kGray+2)
axis3.Draw()

fDiff = ROOT.TF1('fDiff', '[0] + [1]*x + [2]*x**2')#TMath::Exp(-[1]*x) + [2]')#, 0, Lifetime[-1])
fDiff.SetParameter(0, 0.)
fDiff.SetParameter(1, 50.)
fDiff.SetParameter(1, 0.)
fDiff.SetLineStyle(ROOT.kDashed)
fDiff.SetLineColor( ROOT.kGray+2)
fDiff.SetLineWidth(1)
gDiff.Fit('fDiff', '+', '', xRange1, xRange2)

constant = fDiff.GetParameter(0)
constantUnc = fDiff.GetParError(0)
slope = fDiff.GetParameter(1)
slopeUnc = fDiff.GetParError(1)
quad = fDiff.GetParameter(2)
quadUnc = fDiff.GetParError(2)

legend = ROOT.TLegend(0.7, 0.70, 0.88, 0.88)
legend.AddEntry(gRate, 'Relative rate','l')
legend.AddEntry(gDiff, 'Accuracy','l')
legend.AddEntry(fDiff, 'R = b + m t + q t^{2}','l')
legend.AddEntry(fDiff, '    b:  '+str(round(constant,2))+' #pm '+str(round(constantUnc,2))+' s','')
legend.AddEntry(fDiff, '    m:  '+str(round(slope,2))+' #pm '+str(round(slopeUnc,2))+' s/d','')
legend.AddEntry(fDiff, '    q:  '+str(round(quad,2))+' #pm '+str(round(quadUnc,2))+' s/d/d','')
legend.AddEntry(fDiff, '   #chi^{2}/ndf:  '+str(round(fDiff.GetChisquare(),2))+'/'+str(fDiff.GetNDF())+' = '+str(round(fDiff.GetChisquare()/fDiff.GetNDF(),2)),'')
legend.SetFillColor(0);
legend.Draw()

c.SaveAs(logn+str(watches_log_this)+'.png')
'''







### Plot, fit ranges.
xRange12 = RealUTC[0]-(RealUTC[-1]-RealUTC[0])*0.05
xRange22 = RealUTC[-1]+(RealUTC[-1]-RealUTC[0])*0.05
yRange12 = min(Diff_s)-abs(min(Diff_s)*0.1)
yRange22 = max(Diff_s)+abs(max(Diff_s)*0.1)

### Plot Diff vs UTC
c2 = ROOT.TCanvas('c2', 'c2', 1366, 720)
c2.SetGrid()
c2.SetTicks()
gDiff2 = ROOT.TGraphErrors(len(RealUTC), np.array(RealUTC), np.array(Diff_s), np.array(RealUTCUnc), np.array(Diff_sUnc))
gDiff2.SetTitle(watches_name_this+' vs UTC;Elapsed time (d);Accuracy (s)')
gDiff2.GetXaxis().SetLimits(xRange12, xRange22)
gDiff2.GetYaxis().SetRangeUser(yRange12, yRange22)
#gDiff2.SetLineStyle(ROOT.kDashed)
gDiff2.SetLineColor(ROOT.kViolet)
gDiff2.SetLineWidth(2)
#gDiff2.GetXaxis().SetTimeDisplay(1);
#gDiff2.GetXaxis().SetNdivisions(-503);
#gDiff2.GetXaxis().SetTimeFormat("%Y-%m-%d %H:%M");
#gDiff2.GetXaxis().SetTimeOffset(0,'gmt');
gDiff2.Draw()

### Fit Diff
fDiff2 = ROOT.TF1('fDiff2', '[0] + [1]*x + [2]*x**2')
fDiff2.SetParameter(0, 0.)
fDiff2.SetParameter(1, 50.)
fDiff2.SetParameter(1, 0.)
fDiff2.SetLineStyle(ROOT.kDashed)
fDiff2.SetLineColor(ROOT.kGray+2)
fDiff2.SetLineWidth(1)
gDiff2.Fit('fDiff2', '+', '', xRange12, xRange22)

a = fDiff2.GetParameter(0)
aUnc = fDiff2.GetParError(0)
b = fDiff2.GetParameter(1)
bUnc = fDiff2.GetParError(1)
c = fDiff2.GetParameter(2)
cUnc = fDiff2.GetParError(2)

legend2 = ROOT.TLegend(0.2, 0.65, 0.38, 0.83)
#legend2.SetTextFont(132)
legend2.AddEntry(gDiff2, 'Rate','l')
legend2.AddEntry(fDiff2, 'r = a + b t + c t^{2}','l')
legend2.AddEntry(fDiff2, '    a:  '+str(round(a,1))+' #pm '+str(round(aUnc,1))+' s','')
#legend2.SetTextFont(22) # I was trying to make just this b line bold.
legend2.AddEntry(fDiff2, '    b:  '+str(round(b,1))+' #pm '+str(round(bUnc,1))+' s d^{-1}','')
#legend2.SetTextFont(132)
legend2.AddEntry(fDiff2, '    c:  '+str(round(c,1))+' #pm '+str(round(cUnc,1))+' s d^{-2}','')
legend2.AddEntry(fDiff2, '   #chi^{2}/ndf:  '+str(round(fDiff2.GetChisquare(),1))+'/'+str(fDiff2.GetNDF())+' = '+str(round(fDiff2.GetChisquare()/fDiff2.GetNDF(),1)),'')
legend2.SetFillColor(0);
legend2.Draw()

c2.SaveAs(logn+str(watches_log_this)+'.png')







