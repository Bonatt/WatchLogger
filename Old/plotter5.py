import time
import datetime
import os
import ROOT
import numpy as np
import math as math
import linecache
import sys
import glob

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()...
def sqrt(x):
  return np.sqrt(x)

#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(1);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();












WatchLogs = ['skx', 'vostokk', 'vostokb', 'raketa', 'casio']
WatchNames = ['Seiko SKX007', 'Vostok Amphibia, black', 'Vostok Amphibia, blue', 'Raketa Big Zero', 'Casio A158W']

# All this here to indent the print lines so they line up...
lenWatchNames = [len(i) for i in WatchNames]
maxlenWatchNames = max(lenWatchNames)
spaces = [maxlenWatchNames-i for i in lenWatchNames]
indent = [''.ljust(i) for i in spaces]

print ''
for i,n,l in zip(indent,WatchNames, WatchLogs):
  print i, n, '=', '\"'+l+'\"'
print ''

print ' Currently tracked:', ', '.join([g.replace('.log','') for g in glob.glob('*.log')])
print ''


### Create canvas for all plots to be plotted on.
c = ROOT.TCanvas('c', 'c', 1366, 720)
c.SetGrid()
c.SetTicks()

### Create fit functions
pol1 = '[0] + [1]*x'; pol1String = 'a + b t'
pol2 = '[0] + [1]*x + [2]*x**2'; pol2String = 'a + b t + c t^{2}'
pol3 = '[0] + [1]*x + [2]*x**2 + [3]*x**3'; pol3String = 'a + b t + c t^{2} + d t^{3}'
# Change f to any of the above poln
f = pol1
if f == pol1: fString = pol1String
if f == pol2: fString = pol2String
if f == pol3: fString = pol3String
fDiff = ROOT.TF1('fDiff', f)
fDiff.SetParameter(0, 0.)
fDiff.SetParameter(1, 0.)
fDiff.SetParameter(2, 0.)
fDiff.SetParameter(3, 0.)
fDiff.SetLineStyle(ROOT.kDashed)
fDiff.SetLineColor(ROOT.kGray+2)
fDiff.SetLineWidth(1)


### Number of *.log files in directory
nlog = len(glob.glob('*.log'))

### Create legend and legend position. Grows longer towards bottom as nlog increases
x1 = 0.13
x2 = x1+0.18
y2 = 0.87
y1 = 0.13 #y2-nlog/7 #y2-0.2
legend = ROOT.TLegend(x1, y1, x2, y2)


May302017noon = 1496145600-14400 # utc - 14400 s = est
xRangeGraph1 = May302017noon + 2*86400.
xRangeGraph2 = time.time()+60.*30.

line = ROOT.TLine(xRangeGraph1, 0, xRangeGraph2, 0)
line.Draw('same')



### Open and gentxt all existing logs. Plot all data. Fit all data. Don't even append nothing.
gDiff = []
# i here is just for indexing gdiff. Otherwise it would get written/plotted over(?) and you can't start a plot with Draw('same').
i = 0

### Only pulls logs named in above list
#for watchlog in WatchLogs:
#  if os.path.exists(watchlog+'.log'):
#    realUTC, diff = np.genfromtxt(watchlog+'.log', skip_header=1, usecols=(3,4), unpack=True)

### Pulls all logs in directory
for watchlog in [g.replace('.log','') for g in glob.glob('*.log')]:
    realUTC, diff = np.genfromtxt(watchlog+'.log', skip_header=1, usecols=(3,4), unpack=True)

    realUTCUnc = np.zeros(len(realUTC))
    diffUnc = np.empty(len(diff)); diffUnc.fill(0.5)

    gdiff = ROOT.TGraphErrors(len(realUTC), np.array(realUTC), np.array(diff), np.array(realUTCUnc), np.array(diffUnc))
    #if i == 0:
      #yRange1 = min(diff)-abs(min(diff)*0.1)
      #yRange2 = max(diff)+abs(max(diff)*0.1)
      #gdiff.GetYaxis().SetRangeUser(yRange1, yRange2)
    gdiff.GetYaxis().SetRangeUser(-120., 120.) # s
    gdiff.GetXaxis().SetRangeUser(xRangeGraph1, xRangeGraph2)

    gdiff.SetTitle(';;Accuracy (s)')
    gdiff.SetLineColor(2+i)
    gdiff.SetLineWidth(2)

    # Plot UTC --> readable time
    gdiff.GetXaxis().SetTimeDisplay(1)
    gdiff.GetXaxis().SetNdivisions(406) # putting a "-" before the numbers makes it line up exactly?
    gdiff.GetXaxis().SetTimeFormat('#splitline{%Y/%m/%d}{%H:%M}')
    gdiff.GetXaxis().SetTimeOffset(0,'est')
    gdiff.GetXaxis().SetLabelOffset(0.02)
    gdiff.GetYaxis().SetNdivisions(-512)


    gDiff.append(gdiff)

    if i == 0:
      gDiff[i].Draw()
    else:
      gDiff[i].Draw('same')

    ### Fit ranges.
    xRange1 = realUTC[0]-(realUTC[-1]-realUTC[0])*0.05
    xRange2 = realUTC[-1]+(realUTC[-1]-realUTC[0])*0.05
    ### Fit gdiff
    gdiff.Fit('fDiff', 'q+', '', xRange1, xRange2)
    




    ### gDiff again just for fitting and plotting. I can't get x axis (of plot or data) be correct at the same time as fit parameters.
    # I guess this is my way of doing it...
    # Preserve original zeroth index for SetTimeOffset.
    realUTC0 = realUTC[0]
    # Shift UTC value to make first index zero.
    realUTC = [r-realUTC0 for r in realUTC]
   
    gdiffF = ROOT.TGraphErrors(len(realUTC), np.array(realUTC), np.array(diff), np.array(realUTCUnc), np.array(diffUnc))

    ### Fit ranges.
    xRange1 = realUTC[0]-(realUTC[-1]-realUTC[0])*0.05
    xRange2 = realUTC[-1]+(realUTC[-1]-realUTC[0])*0.05
    #yRange1 = min(diff)-abs(min(diff)*0.1)
    #yRange2 = max(diff)+abs(max(diff)*0.1)

    ### Fit gdiffF
    gdiffF.Fit('fDiff', 'q+', '', xRange1, xRange2)

    ### Collect parameters. Multiply by 86400 to change s/s to s/d, e.g.
    a = fDiff.GetParameter(0)
    aUnc = fDiff.GetParError(0)
    b = fDiff.GetParameter(1)*86400.
    bUnc = fDiff.GetParError(1)*86400.
    C = fDiff.GetParameter(2)*86400.*86400.
    CUnc = fDiff.GetParError(2)*86400.*86400.
    d = fDiff.GetParameter(3)*86400.*86400.*86400.
    dUnc = fDiff.GetParError(3)*86400.*86400.*86400.


    # String numbers from WatchLogs_this so skx, skx2, skx3, etc. all pull "Seiko SKX007"
    # From https://stackoverflow.com/questions/12851791/removing-numbers-from-string
    s = watchlog
    s = ''.join([d for d in s if not d.isdigit()])
    s = WatchNames[WatchLogs.index(s)]
     
    ### Add entries to legend
    legend.AddEntry(gdiff, s, 'l')#WatchNames[WatchLogs.index(watchlog)],'l')
    if i == 0:
      legend.AddEntry(fDiff, 'f = '+fString, 'l')
    legend.AddEntry(fDiff, '    a:  '+str(round(a,1))+' #pm '+str(round(aUnc,1))+' s','')
    legend.AddEntry(fDiff, '    b:  '+str(round(b,1))+' #pm '+str(round(bUnc,1))+' s d^{-1}','')
    if f == pol2 or f == pol3:
      legend.AddEntry(fDiff, '    c:  '+str(round(C,1))+' #pm '+str(round(CUnc,1))+' s d^{-2}','')
    if f == pol3:
      legend.AddEntry(fDiff, '    d:  '+str(round(d,1))+' #pm '+str(round(dUnc,1))+' s d^{-3}','')
    legend.AddEntry(fDiff, '   #chi^{2}/ndf:  '+str(round(fDiff.GetChisquare(),1))+'/'+str(fDiff.GetNDF())+' = '+str(round(fDiff.GetChisquare()/fDiff.GetNDF(),1)),'')
    legend.SetFillColor(0);
    #legend.Draw()

 
    i = i + 1

legend.Draw()

#[[i.split('.') for i in glob.glob('*.log')][n][0]+[i.split('.') for i in glob.glob('*.log')][n][0] for n in range(nlog)]
WatchLogs_existAll = ''.join([g.replace('.log','') for g in glob.glob('*.log')])
c.SaveAs(WatchLogs_existAll+'.png')
