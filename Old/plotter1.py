import time
import datetime
import os

import ROOT
import numpy as np
import math as math
import linecache
import sys


# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()...
def sqrt(x):
  return np.sqrt(x)

#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(1);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();












WatchLogs = ['skx', 'vostok_k', 'vostok_b', 'raketa', 'casio']
WatchNames = ['Seiko SKX007', 'Vostok Amphibia, black', 'Vostok Amphibia, blue', 'Raketa Big Zero', 'Casio A158W']

# All this here to indent the print lines so they line up...
lenWatchNames = [len(i) for i in WatchNames]
maxlenWatchNames = max(lenWatchNames)
spaces = [maxlenWatchNames-i for i in lenWatchNames]
indent = [''.ljust(i) for i in spaces]

print ''
for i,n,l in zip(indent,WatchNames, WatchLogs):
  print i, n, '=', '\"'+l+'\"'
print ''



### Open single log
WatchLogs_this = raw_input('plot = ')
WatchNames_this = WatchNames[WatchLogs.index(WatchLogs_this)]


### Open multiple logs
'''
WatchLogs_this = raw_input('plot (multiple separated by comma only) = ')
WatchLogs_this = WatchLogs_this.split(',')

WatchNames_this = []
for i in WatchLogs_this:
  WatchNames_this.append(WatchNames[WatchLogs.index(i)])
'''




RealUTC, Diff = np.genfromtxt(WatchLogs_this+'.log', skip_header=1, usecols=(3,4), unpack=True)

# Shift UTC value to make first index zero.
RealUTC0 = RealUTC[0] # Preserve original zeroth index for SetTimeOffset.
RealUTC = [(i-RealUTC[0])/86400. for i in RealUTC]
# Error is zero.
RealUTCUnc = np.zeros(len(RealUTC))

# Error is half second?
DiffUnc = np.empty(len(Diff))
DiffUnc.fill(0.5)







### Plot, fit ranges.
xRange1 = RealUTC[0]-(RealUTC[-1]-RealUTC[0])*0.05
xRange2 = RealUTC[-1]+(RealUTC[-1]-RealUTC[0])*0.05
yRange1 = min(Diff)-abs(min(Diff)*0.1)
yRange2 = max(Diff)+abs(max(Diff)*0.1)

### Plot Diff vs UTC
c = ROOT.TCanvas('c', 'c', 1366, 720)
c.SetGrid()
c.SetTicks()
gDiff = ROOT.TGraphErrors(len(RealUTC), np.array(RealUTC), np.array(Diff), np.array(RealUTCUnc), np.array(DiffUnc))
gDiff.SetTitle(WatchNames_this+' vs UTC;Elapsed time (d);Accuracy (s)')
gDiff.GetXaxis().SetLimits(xRange1, xRange2)
gDiff.GetYaxis().SetRangeUser(yRange1, yRange2)
gDiff.SetLineColor(ROOT.kViolet)
gDiff.SetLineWidth(2)
# Plot UTC --> readable time
gDiff.GetXaxis().SetTimeDisplay(1)
gDiff.GetXaxis().SetNdivisions(-503)
gDiff.GetXaxis().SetTimeFormat("%Y/%m/%d %H:%M")
gDiff.GetXaxis().SetTimeOffset(RealUTC0,'est')
gDiff.Draw()

### Fit Diff
pol1 = '[0] + [1]*x'; pol1String = 'a + b t'
pol2 = '[0] + [1]*x + [2]*x**2'; pol2String = 'a + b t + c t^{2}'
pol3 = '[0] + [1]*x + [2]*x**2 + [3]*x**3'; pol3String = 'a + b t + c t^{2} + d t^{3}'

# Change f to any of the above polN
f = pol2
if f == pol1: fString = pol1String
if f == pol2: fString = pol2String
if f == pol3: fString = pol3String
fDiff = ROOT.TF1('fDiff', f)
fDiff.SetParameter(0, 0.)
fDiff.SetParameter(1, 0.)
fDiff.SetParameter(2, 0.)
fDiff.SetParameter(3, 0.)
fDiff.SetLineStyle(ROOT.kDashed)
fDiff.SetLineColor(ROOT.kGray+2)
fDiff.SetLineWidth(1)
gDiff.Fit('fDiff', '+', '', xRange1, xRange2)


a = fDiff.GetParameter(0)
aUnc = fDiff.GetParError(0)
b = fDiff.GetParameter(1)
bUnc = fDiff.GetParError(1)
C = fDiff.GetParameter(2)
CUnc = fDiff.GetParError(2)
d = fDiff.GetParameter(3)
dUnc = fDiff.GetParError(3)

legend = ROOT.TLegend(0.2, 0.65, 0.38, 0.83)
legend.AddEntry(gDiff, 'Rate','l')
legend.AddEntry(fDiff, 'f = '+fString, 'l')
legend.AddEntry(fDiff, '    a:  '+str(round(a,1))+' #pm '+str(round(aUnc,1))+' s','')
legend.AddEntry(fDiff, '    b:  '+str(round(b,1))+' #pm '+str(round(bUnc,1))+' s d^{-1}','')
if f == pol2 or f == pol3:
  legend.AddEntry(fDiff, '    c:  '+str(round(C,1))+' #pm '+str(round(CUnc,1))+' s d^{-2}','')
if f == pol3:
  legend.AddEntry(fDiff, '    d:  '+str(round(d,1))+' #pm '+str(round(dUnc,1))+' s d^{-3}','')
legend.AddEntry(fDiff, '   #chi^{2}/ndf:  '+str(round(fDiff.GetChisquare(),1))+'/'+str(fDiff.GetNDF())+' = '+str(round(fDiff.GetChisquare()/fDiff.GetNDF(),1)),'')
legend.SetFillColor(0);
legend.Draw()

c.SaveAs(WatchLogs_this+'.png')







