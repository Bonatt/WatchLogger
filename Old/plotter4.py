import time
import datetime
import os
import ROOT
import numpy as np
import math as math
import linecache
import sys
import glob

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()...
def sqrt(x):
  return np.sqrt(x)

#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(1);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();












WatchLogs = ['skx', 'vostok_k', 'vostok_b', 'raketa', 'casio']
WatchNames = ['Seiko SKX007', 'Vostok Amphibia, black', 'Vostok Amphibia, blue', 'Raketa Big Zero', 'Casio A158W']

# All this here to indent the print lines so they line up...
lenWatchNames = [len(i) for i in WatchNames]
maxlenWatchNames = max(lenWatchNames)
spaces = [maxlenWatchNames-i for i in lenWatchNames]
indent = [''.ljust(i) for i in spaces]

print ''
for i,n,l in zip(indent,WatchNames, WatchLogs):
  print i, n, '=', '\"'+l+'\"'
print ''



### Open single log
'''
WatchLogs_this = raw_input('plot = ')
WatchNames_this = WatchNames[WatchLogs.index(WatchLogs_this)]

RealUTC, Diff = np.genfromtxt(WatchLogs_this+'.log', skip_header=1, usecols=(3,4), unpack=True)
'''

### Open multiple logs. Not really working at all.
'''
WatchLogs_this = raw_input('plot (multiple separated by comma only) = ')
WatchLogs_this = WatchLogs_this.split(',')

WatchNames_this = []
for i in WatchLogs_this:
  WatchNames_this.append(WatchNames[WatchLogs.index(i)])

RealUTC = np.empty(len(WatchLogs_this))
Diff = np.empty(len(WatchLogs_this))
for i,watchlogs_this in enumerate(WatchLogs_this):
  RealUTC[i], Diff[i] = np.genfromtxt(watchlogs_this+'.log', skip_header=1, usecols=(3,4), unpack=True)
'''


### Open and gentxt all existing logs
'''
WatchLogs_exist = []
WatchNames_exist = []
RealUTC = []
RealUTCUnc = []
Diff = []
DiffUnc = []
for watchlog in WatchLogs:
  if os.path.exists(watchlog+'.log'):
    WatchLogs_exist.append(watchlog)
    WatchNames_exist.append(WatchNames[WatchLogs.index(watchlog)])

    realUTC, diff = np.genfromtxt(watchlog+'.log', skip_header=1, usecols=(3,4), unpack=True)
    RealUTC.append(realUTC)
    RealUTCUnc.append(np.zeros(len(realUTC)))

    Diff.append(diff)    
    diffUnc = np.empty(len(diff))
    diffUnc.fill(0.5)
    DiffUnc.append(diffUnc)




c = ROOT.TCanvas('c', 'c', 1366, 720)
c.SetGrid()
c.SetTicks()

gDiff = []
for i in range(len(WatchLogs_exist)):
  
  gdiff = ROOT.TGraphErrors(len(RealUTC[i]), np.array(RealUTC[i]), np.array(Diff[i]), np.array(RealUTCUnc[i]), np.array(DiffUnc[i]))
  #gdiff.GetXaxis().SetLimits(xRange1, xRange2)
  #gdiff.GetYaxis().SetRangeUser(yRange1, yRange2)
  gdiff.SetLineColor(2+i)
  gdiff.SetLineWidth(2)

  # Plot UTC --> readable time
  gdiff.GetXaxis().SetTimeDisplay(1)
  gdiff.GetXaxis().SetNdivisions(-503)
  gdiff.GetXaxis().SetTimeFormat('#splitline{%Y/%m/%d}{%H:%M}')
  gdiff.GetXaxis().SetTimeOffset(0,'est')
  gdiff.GetXaxis().SetLabelOffset(0.02)
 
  if i == 0:
    gdiff.SetTitle(';;Accuracy (s)')
    gdiff.Draw()
  else:
    gdiff.Draw('same')

  gDiff.append(gdiff)
'''












### Create canvas for all plots to be plotted on.
c = ROOT.TCanvas('c', 'c', 1366, 720)
c.SetGrid()
c.SetTicks()


### Create fit functions
pol1 = '[0] + [1]*x'; pol1String = 'a + b t'
pol2 = '[0] + [1]*x + [2]*x**2'; pol2String = 'a + b t + c t^{2}'
pol3 = '[0] + [1]*x + [2]*x**2 + [3]*x**3'; pol3String = 'a + b t + c t^{2} + d t^{3}'
# Change f to any of the above polN
f = pol2
if f == pol1: fString = pol1String
if f == pol2: fString = pol2String
if f == pol3: fString = pol3String
fDiff = ROOT.TF1('fDiff', f)
fDiff.SetParameter(0, 0.)
fDiff.SetParameter(1, 0.)
fDiff.SetParameter(2, 0.)
fDiff.SetParameter(3, 0.)
fDiff.SetLineStyle(ROOT.kDashed)
fDiff.SetLineColor(ROOT.kGray+2)
fDiff.SetLineWidth(1)


### Number of *.log files in directory
nlog = len(glob.glob('*.log'))

### Create legend and legend position. Grows longer towards bottom as nlog increases
x1 = 0.15
x2 = x1+0.2
y2 = 0.85
#y1 = y2-0.2
legend = ROOT.TLegend(x1, y2-nlog/7., x2, y2)


### Open and gentxt all existing logs. Plot all data. Fit all data. Don't even append nothing.
#WatchLogs_exist = []
#WatchNames_exist = []
#RealUTC = []
#RealUTCUnc = []
#Diff = []
#DiffUnc = []

gDiff = []
i = 0
for watchlog in WatchLogs:
  if os.path.exists(watchlog+'.log'):

    realUTC, diff = np.genfromtxt(watchlog+'.log', skip_header=1, usecols=(3,4), unpack=True)

    realUTCUnc = np.zeros(len(realUTC))
    diffUnc = np.empty(len(diff)); diffUnc.fill(0.5)

    gdiff = ROOT.TGraphErrors(len(realUTC), np.array(realUTC), np.array(diff), np.array(realUTCUnc), np.array(diffUnc))
    gdiff.SetTitle(';;Accuracy (s)')
    gdiff.SetLineColor(2+i)
    gdiff.SetLineWidth(2)

    # Plot UTC --> readable time
    gdiff.GetXaxis().SetTimeDisplay(1)
    gdiff.GetXaxis().SetNdivisions(-503)
    gdiff.GetXaxis().SetTimeFormat('#splitline{%Y/%m/%d}{%H:%M}')
    gdiff.GetXaxis().SetTimeOffset(0,'est')
    gdiff.GetXaxis().SetLabelOffset(0.02)

    gDiff.append(gdiff)

    if i == 0:
      gDiff[i].Draw()
    else:
      gDiff[i].Draw('same')



    ### Fit ranges.
    xRange1 = realUTC[0]-(realUTC[-1]-realUTC[0])*0.05
    xRange2 = realUTC[-1]+(realUTC[-1]-realUTC[0])*0.05
    #yRange1 = min(diff)-abs(min(diff)*0.1)
    #yRange2 = max(diff)+abs(max(diff)*0.1)
    gdiff.Fit('fDiff', '+', '', xRange1, xRange2)

    # Collect parameters. Multiply by 86400 to change s/s to s/d, e.g.
    a = fDiff.GetParameter(0)
    aUnc = fDiff.GetParError(0)
    b = fDiff.GetParameter(1)*86400.
    bUnc = fDiff.GetParError(1)*86400.
    C = fDiff.GetParameter(2)*86400.*86400.
    CUnc = fDiff.GetParError(2)*86400.*86400.
    d = fDiff.GetParameter(3)*86400.*86400.*86400.
    dUnc = fDiff.GetParError(3)*86400.*86400.*86400.


    #legend = ROOT.TLegend(0.2, 0.65, 0.38, 0.83)
    legend.AddEntry(gdiff, WatchNames[WatchLogs.index(watchlog)],'l')
    if i == 0:
      legend.AddEntry(fDiff, 'f = '+fString, 'l')
    legend.AddEntry(fDiff, '    a:  '+str(round(a,1))+' #pm '+str(round(aUnc,1))+' s','')
    legend.AddEntry(fDiff, '    b:  '+str(round(b,1))+' #pm '+str(round(bUnc,1))+' s d^{-1}','')
    if f == pol2 or f == pol3:
      legend.AddEntry(fDiff, '    c:  '+str(round(C,1))+' #pm '+str(round(CUnc,1))+' s d^{-2}','')
    if f == pol3:
      legend.AddEntry(fDiff, '    d:  '+str(round(d,1))+' #pm '+str(round(dUnc,1))+' s d^{-3}','')
    legend.AddEntry(fDiff, '   #chi^{2}/ndf:  '+str(round(fDiff.GetChisquare(),1))+'/'+str(fDiff.GetNDF())+' = '+str(round(fDiff.GetChisquare()/fDiff.GetNDF(),1)),'')
    legend.SetFillColor(0);
    legend.Draw()

    











    i = i + 1


'''
gDiff = []
for i in range(len(WatchLogs_exist)):
  
  gdiff = ROOT.TGraphErrors(len(RealUTC[i]), np.array(RealUTC[i]), np.array(Diff[i]), np.array(RealUTCUnc[i]), np.array(DiffUnc[i]))
  #gdiff.GetXaxis().SetLimits(xRange1, xRange2)
  #gdiff.GetYaxis().SetRangeUser(yRange1, yRange2)
  gdiff.SetLineColor(2+i)
  gdiff.SetLineWidth(2)

  # Plot UTC --> readable time
  gdiff.GetXaxis().SetTimeDisplay(1)
  gdiff.GetXaxis().SetNdivisions(-503)
  gdiff.GetXaxis().SetTimeFormat('#splitline{%Y/%m/%d}{%H:%M}')
  gdiff.GetXaxis().SetTimeOffset(0,'est')
  gdiff.GetXaxis().SetLabelOffset(0.02)
 
  if i == 0:
    gdiff.SetTitle(';;Accuracy (s)')
    gdiff.Draw()
  else:
    gdiff.Draw('same')

  gDiff.append(gdiff)
'''









































'''
# Legend
x1 = 0.75
y1 = 0.6
x2 = 0.89
y2 = 0.89
legend = ROOT.TLegend(x1, y1, x2, y2)
legend.AddEntry(hStackSum,  'Experimental, '+str('{:,}'.format(int(hStack.GetStack().Last().GetEntries()))), 'l')

# Fills legend with games
hStackList = hStack.GetHists()
for i in range(N+1):
  legend.AddEntry(hStackList[i], '     Game'+str(i+1)+', '+str('{:,}'.format(int(hStackList[i].GetEntries()))), 'f')

legend.AddEntry(hTheory,  'Theoretical, '+str(int(nd/1e6))+'e6', 'l')
legend.SetFillColor(0)
legend.Draw()
'''





#gDiff[0].Draw()
#gDiff[1].Draw('same')


'''
gDiff0 = ROOT.TGraphErrors(len(RealUTC[0]), np.array(RealUTC[0]), np.array(Diff[0]), np.array(RealUTCUnc[0]), np.array(DiffUnc[0]))
gDiff0.Draw()
gDiff1 = ROOT.TGraphErrors(len(RealUTC[1]), np.array(RealUTC[1]), np.array(Diff[1]), np.array(RealUTCUnc[1]), np.array(DiffUnc[1]))
gDiff1.Draw('same')
'''












'''
# Preserve original zeroth index for SetTimeOffset.
RealUTC0 = RealUTC[0]
# Shift UTC value to make first index zero.
#RealUTC = [(i-RealUTC[0])/86400. for i in RealUTC]
RealUTC = [(i-RealUTC[0]) for i in RealUTC]
# Error is zero.
RealUTCUnc = np.zeros(len(RealUTC))

# Error is half second?
DiffUnc = np.empty(len(Diff))
DiffUnc.fill(0.5)







### Plot, fit ranges.
xRange1 = RealUTC[0]-(RealUTC[-1]-RealUTC[0])*0.05
xRange2 = RealUTC[-1]+(RealUTC[-1]-RealUTC[0])*0.05
yRange1 = min(Diff)-abs(min(Diff)*0.1)
yRange2 = max(Diff)+abs(max(Diff)*0.1)

### Plot Diff vs UTC
c = ROOT.TCanvas('c', 'c', 1366, 720)
c.SetGrid()
c.SetTicks()
gDiff = ROOT.TGraphErrors(len(RealUTC), np.array(RealUTC), np.array(Diff), np.array(RealUTCUnc), np.array(DiffUnc))
#gDiff.SetTitle(WatchNames_this+' vs UTC;Elapsed time (d);Accuracy (s)')
gDiff.SetTitle(WatchNames_this+' vs UTC;;Accuracy (s)')
gDiff.GetXaxis().SetLimits(xRange1, xRange2)
gDiff.GetYaxis().SetRangeUser(yRange1, yRange2)
gDiff.SetLineColor(ROOT.kViolet)
gDiff.SetLineWidth(2)
# Plot UTC --> readable time
gDiff.GetXaxis().SetTimeDisplay(1)
gDiff.GetXaxis().SetNdivisions(-503)
gDiff.GetXaxis().SetTimeFormat('#splitline{%Y/%m/%d}{%H:%M}')
gDiff.GetXaxis().SetTimeOffset(RealUTC0,'est')
gDiff.GetXaxis().SetLabelOffset(0.02)
gDiff.Draw()

### Fit Diff
pol1 = '[0] + [1]*x'; pol1String = 'a + b t'
pol2 = '[0] + [1]*x + [2]*x**2'; pol2String = 'a + b t + c t^{2}'
pol3 = '[0] + [1]*x + [2]*x**2 + [3]*x**3'; pol3String = 'a + b t + c t^{2} + d t^{3}'
# Change f to any of the above polN
f = pol2
if f == pol1: fString = pol1String
if f == pol2: fString = pol2String
if f == pol3: fString = pol3String
fDiff = ROOT.TF1('fDiff', f)
fDiff.SetParameter(0, 0.)
fDiff.SetParameter(1, 0.)
fDiff.SetParameter(2, 0.)
fDiff.SetParameter(3, 0.)
fDiff.SetLineStyle(ROOT.kDashed)
fDiff.SetLineColor(ROOT.kGray+2)
fDiff.SetLineWidth(1)
gDiff.Fit('fDiff', '+', '', xRange1, xRange2)


# Collect parameters. Multiply by 86400 to change s/s to s/d, e.g.
a = fDiff.GetParameter(0)
aUnc = fDiff.GetParError(0)
b = fDiff.GetParameter(1)*86400.
bUnc = fDiff.GetParError(1)*86400.
C = fDiff.GetParameter(2)*86400.*86400.
CUnc = fDiff.GetParError(2)*86400.*86400.
d = fDiff.GetParameter(3)*86400.*86400.*86400.
dUnc = fDiff.GetParError(3)*86400.*86400.*86400.

legend = ROOT.TLegend(0.2, 0.65, 0.38, 0.83)
legend.AddEntry(gDiff, 'Rate','l')
legend.AddEntry(fDiff, 'f = '+fString, 'l')
legend.AddEntry(fDiff, '    a:  '+str(round(a,1))+' #pm '+str(round(aUnc,1))+' s','')
legend.AddEntry(fDiff, '    b:  '+str(round(b,1))+' #pm '+str(round(bUnc,1))+' s d^{-1}','')
if f == pol2 or f == pol3:
  legend.AddEntry(fDiff, '    c:  '+str(round(C,1))+' #pm '+str(round(CUnc,1))+' s d^{-2}','')
if f == pol3:
  legend.AddEntry(fDiff, '    d:  '+str(round(d,1))+' #pm '+str(round(dUnc,1))+' s d^{-3}','')
legend.AddEntry(fDiff, '   #chi^{2}/ndf:  '+str(round(fDiff.GetChisquare(),1))+'/'+str(fDiff.GetNDF())+' = '+str(round(fDiff.GetChisquare()/fDiff.GetNDF(),1)),'')
legend.SetFillColor(0);
legend.Draw()

c.SaveAs(WatchLogs_this+'.png')
#c.SaveAs(WatchLogs_existAll+'.png')
'''
















